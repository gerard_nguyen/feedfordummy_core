<?php

/*** FUNCTIONS DEF***/

//function to recursively merge two array
function array_merge_recursive_distinct(array &$array1, array &$array2)
{
    $merged = $array1;

    foreach ($array2 as $key => &$value) {
        if (is_array($value) && isset($merged [$key]) && is_array($merged [$key])) {
            $merged [$key] = array_merge_recursive_distinct($merged [$key], $value);
        } else {
            $merged [$key] = $value;
        }
    }

    return $merged;
}

//function to convert 1d array to hierachy array and assign category level
function createHierachArray($arr = [], $catLevel = 1)
{
    if (count($arr) == 0) {
        return $arr;
    }

    $key1 = $arr[0];

    if (count($arr) == 1) {
        return ["_catLevel" => $catLevel, $key1 => ""];
    }
    
    $key2 = $arr[1];

    if (count($arr) == 2) {
        return [ "_catLevel" => $catLevel, $key1 => ["_catLevel" => $catLevel + 1, $key2 => ""]];
    }

    return ["_catLevel" => $catLevel, $key1 => createHierachArray(
        array_slice($arr, 1),
        $catLevel + 1
    )];
}

//function to convert a line to hierachy array
function convertLinetoHierachyArray($line)
{
    $arr = [];

    //split into 1d array separated by '>'
    $arr = explode(" > ", $line);

    return createHierachArray($arr);
}

/*** MAIN PROCESSING LOGIC ***/

//read taxonomy file
$final_arr = [];
$taxonomy_file = fopen("taxonomy.en-US.txt", "r") or die("Unable to open taxonomy file!");

//read each taxonomy line
fgets($taxonomy_file); //skip 1st line
while (!feof($taxonomy_file)) {
    $line = trim(fgets($taxonomy_file));

    if (empty($line)) {
        continue;
    }

    //convert line to hierachy array
    $arr = convertLinetoHierachyArray($line);
    $final_arr = array_merge_recursive_distinct($final_arr, $arr);
}

//close taxonomy file
fclose($taxonomy_file);

//convert to json
$json_file = json_encode($final_arr);

//save to json file
file_put_contents("taxonomy.en-US.json", $json_file);
